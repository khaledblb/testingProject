import pytest
import requests

@pytest.mark.positive_test
def test_pageContentIsExist():
    url = "https://the-internet.herokuapp.com/context_menu"
    page = requests.get(url)

    assert "Right-click in the box below to see one called 'the-internet'" in page.text

@pytest.mark.negative_test
def test_pageContainString(str="Alibaba") :
    url = "https://the-internet.herokuapp.com/context_menu"
    page = requests.get(url)

    assert str in page.text
